import Configs from "../Config/Configs";
import Settings from "../State/Settings";

export default class TextButton extends Phaser.GameObjects.Text {
	constructor(scene, x, y, text, style, pointerUpCallback) {
		super(scene, x, y, text, style);
		scene.add.existing(this);

		this.setOrigin(0.5);
		this.setInteractive({ useHandCursor: true, });
		this.on("pointerover", () => this.setScale(1.05));
		this.on("pointerout", () => this.setScale(1));
		this.on("pointerup", () => pointerUpCallback());
	}
}
