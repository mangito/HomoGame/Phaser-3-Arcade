import Configs from "../Config/Configs";
import Settings from "../State/Settings";

import EventManager from "../State/EventManager";

import { TextStyle } from "../Theme";

import ModalBG from "../Components/ModalBG";
import TextButton from "../Components/TextButton";

export default class Leaderboard extends Phaser.Scene {
	constructor() {
		super({ key: "Leaderboard", });
	}

	init() {
		this.Settings = new Settings();
	}

	create() {
		const { width, height, middleWidth, middleHeight } = Configs.screen;

		{ // Inputs
			const exitKey = this.input.keyboard.addKey("ESC");
			exitKey.removeAllListeners();
			exitKey.on("down", this.returnHome, this);
		}

		const modalBG = new ModalBG(this);
		modalBG.addTitle("leaderboard");

		const leaderboard = this.Settings.getTopScore(10);
		if (leaderboard.length < 1) {
			this.add.text(middleWidth, middleHeight, this.Settings.output.noLeaderboard, TextStyle.leaderboard.noLeaderboard).setOrigin(0.5, 0.5);
		} else {
			leaderboard.forEach((data, index) => {
				this.add.text(middleWidth, 200 + index * 50, `${index + 1}. ${this.Settings.output[data.levelName]}: ${data.score}  (${data.date})`, TextStyle.leaderboard.leaderboard).setOrigin(0.5, 0.5).setColor(TextStyle.levelsNamesBaseColors[data.levelName]);
			});
		}

		// ------ Footer -> Back Game Button
		const backBTN = new TextButton(this, 50, height - 50, this.Settings.output.back, TextStyle.settings.back,
			() => {
				this.returnHome()
			})
			.setOrigin(0, 0.5);
	}

	returnHome() {
		EventManager.emit("ReactivateHomeButtons");
		this.scene.stop();
	}
}
