import Configs from "../Config/Configs";
import Settings from "../State/Settings";
import { TextStyle } from "../Theme";

import Apple from "../Objects/Apple";
import Duke from "../Objects/Duke";
import Platforms from "../Objects/Platforms";
import Player from "../Objects/Player";

import PowerUp from "../Objects/PowerUp/PowerUp";

export default class Game extends Phaser.Scene {
	constructor() {
		super({ key: "Game", });
	}

	init(data) {
		this.Settings = new Settings();

		this.score = 0;
		this.levelConfig = data.levelConfig;
	}

	create() {
		const { width, height, middleWidth, middleHeight } = Configs.screen;

		// Platforms
		this.platformGroup = new Platforms(this.physics.world, this);

		// Player
		const playersGroup = this.physics.add.group({
			classType: Player,
			collideWorldBounds: true,
			runChildUpdate: true,
		});
		this.player = playersGroup.get(middleWidth, middleHeight + 50);

		// Apple
		this.appleGroup = new Apple(this.physics.world, this);

		// Duke
		this.dukeGroup = new Duke(this.physics.world, this);

		// PowerUp
		if (this.levelConfig?.powerUp) {
			this.powerUpGroup = new PowerUp(this.physics.world, this);
		}

		// Label
		this.scoreLabel = this.add.text(middleWidth, 50, `${this.score}`, TextStyle.game.score).setOrigin(0.5).setDepth(2);

		// Collisions
		this.physics.add.collider(this.player, this.platformGroup);
		this.physics.add.collider(this.player, this.appleGroup, this.addScore, null, this);
		if (this.levelConfig?.powerUp) this.physics.add.collider(this.player, this.powerUpGroup, this.transformAll, null, this);
		this.physics.add.collider(this.player, this.dukeGroup, this.gameOver, null, this);

		// Respawn Apple
		this.physics.add.overlap(this.appleGroup, this.appleGroup, (a1, a2) => a1.updatePosition());
		this.physics.add.overlap(this.platformGroup, this.appleGroup, (p, a) => a.updatePosition());
		this.physics.add.overlap(this.dukeGroup, this.appleGroup, (d, a) => { if (d.alive) a.updatePosition(); });

		// Respawn PowerUp
		if (this.levelConfig?.powerUp) {
			this.physics.add.overlap(this.platformGroup, this.powerUpGroup, (p, pu) => pu.updatePosition());
			this.physics.add.overlap(this.appleGroup, this.powerUpGroup, (a, pu) => pu.updatePosition());
			this.physics.add.overlap(this.dukeGroup, this.powerUpGroup, (d, pu) => pu.updatePosition());
		}

		// KeyBoard Input
		const keyR = this.input.keyboard.addKey("R"); // Restart
		const keysPause = this.input.keyboard.addKeys("P, ESC"); // Pause

		keyR.removeAllListeners();
		keysPause.P.removeAllListeners();
		keysPause.ESC.removeAllListeners();

		keyR.on("down", () => this.scene.restart());
		keysPause.P.on("down", this.pauseGame, this);
		keysPause.ESC.on("down", this.pauseGame, this);

		if (this.levelConfig?.dark) this.activateLights();
	}

	activateLights() {
		this.lights.enable().setAmbientColor(0x000000);
		this.platformGroup.activateLights();
		this.player.activateLights();
		this.appleGroup.activateLights();
		this.dukeGroup.activateLights();
		// this.powerUpGroup.activateLights();
	}

	addScore(p, a) {
		const { alive } = a;
		if (!alive || !this.player.alive) return;

		this.score++;
		this.scoreLabel.setText(`${this.score}`);

		this.player.explodePoint();
		a.explode();
	}

	transformAll(p, pu) {
		if (!pu.alive || !this.player.alive) return;
		pu.kill();

		this.dukeGroup.getChildren().forEach(duke => {
			const { x, y } = duke;
			this.appleGroup.createNewApple(x, y, false);
			duke.kill();
		});
	}

	gameOver(p, d) {
		if (!this.player.alive && d.alive) return;
		this.physics.pause();
		this.player.kill();
		d.kill();

		this.time.delayedCall(Configs.timers.restart, () => {
			const data = {
				levelConfig: this.levelConfig,
				score: this.score,
			};
			this.scene.launch("GameOver", data);
			this.scene.pause();
		});
	}

	pauseGame() {
		if (!this.player.alive) return;
		this.scene.launch("Pause");
		this.scene.pause();
	}
}
