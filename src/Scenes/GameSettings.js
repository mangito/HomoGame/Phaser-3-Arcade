import Configs from "../Config/Configs";
import Settings from "../State/Settings";

import EventManager from "../State/EventManager";

import { TextStyle } from "../Theme";

import ModalBG from "../Components/ModalBG";
import TextButton from "../Components/TextButton";
export default class GameSettings extends Phaser.Scene {
	constructor() {
		super({ key: "GameSettings", });
	}

	init() {
		this.Settings = new Settings();
	}

	create() {
		const { width, height, middleWidth, middleHeight } = Configs.screen;

		{ // Inputs
			const exitKey = this.input.keyboard.addKey("ESC");
			exitKey.removeAllListeners();
			exitKey.on("down", this.returnHome, this);
		}

		// ------ Header
		const modalBG = new ModalBG(this);
		modalBG.addTitle("settings");

		// ------ Main
		// FullScreen Button
		const soundBTN = new TextButton(this, middleWidth, middleHeight - 60, this.Settings.output.sound, TextStyle.settings.menu,
			() => {
				this.game.sound.mute = !this.game.sound.mute;
				this.scene.restart();
			})
			.removeInteractive()
			.setColor(TextStyle.settings.baseColor.disabled);
		// .setColor(TextStyle.settings.baseColor[!this.game.sound.mute]);

		// FullScreen Button
		const fullScreenBTN = new TextButton(this, middleWidth, middleHeight, this.Settings.output.fullScreen, TextStyle.settings.menu,
			() => {
				this.scale.toggleFullscreen();
				this.time.delayedCall(10, () => this.scene.restart());
			})
			.setColor(TextStyle.settings.baseColor[this.scale.isFullscreen]);

		// Language Button
		const languageBTN = new TextButton(this, middleWidth, middleHeight + 60, this.Settings.output.language, TextStyle.settings.menu,
			() => {
				this.Settings.language = (this.Settings.language === "pt" ? "en" : "pt");
				EventManager.emit("LanguageChanged");
				this.scene.restart();
			});


		// ------ Footer -> Back Game Button
		const backBTN = new TextButton(this, 50, height - 50, this.Settings.output.back, TextStyle.settings.back,
			() => {
				this.returnHome()
			})
			.setOrigin(0, 0.5);
	}

	returnHome() {
		EventManager.emit("ReactivateHomeButtons");
		this.scene.stop();
	}
}
