const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());

const GlobalConfigs = {
	screen: {
		width: 1280, // 16:9 // https://calculateaspectratio.com
		height: 720,
		middleWidth: 0,
		middleHeight: 0,
	},
	debug: params.debug !== "true" ? false : true,
	language: "en",
	controllers: {
		keyboard: {
			left1: "LEFT",
			left2: "A",
			right1: "RIGHT",
			right2: "D",
			up1: "W",
			up2: "UP",
			up3: "SPACE",
			down1: "S",
			down2: "DOWN",
		},
		gamePad: {
			left: 14,
			right: 15,
			down: 13,
			up1: 12,
			up2: 0, // X / A
			up3: 1, // Circle / B
			up4: 2, // Square / X
			up5: 3, // Triangle / Y
		},
	},
	timers: {
		restart: 2000,
	},
	powerUps: {
		speed: "SPEED",
		removeHalf: "REMOVE_HALF",
		transformAll: "TRANSFORM_ALL",
	},
	levels: {
		base: {
			powerUp: true,
			dark: false,
			movePlatforms: false,
			moveEnemies: false,
			bigMap: false,
		},
		easy: {},
		normal: {},
		hard: {},
	},
};

// Screen
GlobalConfigs.screen.middleWidth = GlobalConfigs.screen.width / 2;
GlobalConfigs.screen.middleHeight = GlobalConfigs.screen.height / 2;

// Levels
GlobalConfigs.levels.easy = { ...GlobalConfigs.levels.base, };
GlobalConfigs.levels.normal = {
	...GlobalConfigs.levels.base,
	powerUp: false,
};

GlobalConfigs.levels.hard = {
	...GlobalConfigs.levels.base,
	powerUp: false,
	dark: true,
};

export default GlobalConfigs;
