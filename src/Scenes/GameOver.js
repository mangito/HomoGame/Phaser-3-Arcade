import Configs from "../Config/Configs";
import Settings from "../State/Settings";

import { getDateFormatted } from "../Utils/Utils";

import { TextStyle } from "../Theme";

import ModalBG from "../Components/ModalBG";
import TextButton from "../Components/TextButton";

export default class GameOver extends Phaser.Scene {
	constructor() {
		super({ key: "GameOver", });
	}

	init(data) {
		this.Settings = new Settings();

		this.gameInfo = data;

		this.Settings.addScore({
			levelName: data.levelConfig.levelName,
			score: data.score,
			date: getDateFormatted(),
		});
	}

	create() {
		const { width, height, middleWidth, middleHeight } = Configs.screen;

		{ // Inputs
			const escKey = this.input.keyboard.addKey("ESC");
			escKey.removeAllListeners();
			escKey.on("down", this.backToHome, this);
		}

		const modalBG = new ModalBG(this);
		modalBG.addTitle("gameOver");

		{ // Main info
			const output = this.Settings.output;
			const levelName = this.gameInfo.levelConfig.levelName;

			// Level Name
			this.add.text(middleWidth, 125, output[levelName], TextStyle.gameOver.levelName)
				.setOrigin(0.5)
				.setColor(TextStyle.levelsNamesBaseColors[levelName]);

			// Score
			this.add.text(middleWidth, middleHeight, `${output.score}: ${this.gameInfo.score}`, TextStyle.gameOver.score)
				.setOrigin(0.5);

			// Best Score
			this.add.text(middleWidth, middleHeight + 50, `${output.bestScore}: ${this.Settings.getBestScore()}`, TextStyle.gameOver.bestScore)
				.setOrigin(0.5);
		}

		const playAgainBTN = new TextButton(this, width - 50, height - 50, this.Settings.output.playAgain, TextStyle.gameOver.playAgain, () => this.playAgain())
			.setOrigin(1, 0.5);

		const homeBTN = new TextButton(this, 50, height - 50, this.Settings.output.back, TextStyle.gameOver.back, () => this.backToHome())
			.setOrigin(0, 0.5);
	}

	playAgain() {
		this.scene.stop("Game");
		this.scene.start("Game", { levelConfig: this.gameInfo.levelConfig, });
		this.scene.stop();
	}

	backToHome() {
		this.scene.start("Home");
		this.scene.stop("Game");
		this.scene.stop();
	}
}
