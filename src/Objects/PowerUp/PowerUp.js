import Configs from "../../Config/Configs";

export class PowerUp extends Phaser.Physics.Arcade.Sprite {
	constructor(scene, type) {
		let texture;
		if (type === Configs.powerUps.transformAll) texture = "PowerUp";
		super(scene, 0, 0, texture);

		this.alive = true;
		this.type = type;

		this.setVisible(false);

		const spawnDelay = 5 * 1000;
		this.killTimer = this.scene.time.delayedCall(spawnDelay, () => this.kill());

		this.scene.tweens.add({
			targets: [this],
			ease: "Linear",
			duration: 100,
			delay: 50,
			scale: { from: 0.75, to: 1, },
			alpha: { from: 0.75, to: 1, },
			onStart: () => this.setVisible(true),
			onUpdate: () => this.refreshBody(),
		});
	}

	updatePosition() {
		if (!this.alive, !this.scene) return;
		const { width, height, middleWidth, middleHeight } = Configs.screen;
		const marginSize = this.width / 2;
		this.setRandomPosition(marginSize, marginSize, width - this.width, height - this.width);
		this.refreshBody();
	}

	kill() {
		const { alive, scene, killTimer } = this;
		if (!alive, !scene) return;
		this.alive = false;

		this.disableBody();
		killTimer.destroy();

		this.setAlpha(0.5);

		this.scene.tweens.add({
			targets: [this],
			delay: 50,
			duration: 500,
			ease: "Linear",
			scale: { from: 1, to: 0, },
			completeDelay: 100,
			onComplete: () => this.destroy(),
		});
	}
}

export default class PowerUpGroup extends Phaser.Physics.Arcade.StaticGroup {
	constructor(world, scene) {
		const config = {
			classType: PowerUp,
			maxSize: 1,
			collideWorldBounds: true,
		};
		super(world, scene, config);

		scene.time.addEvent({
			delay: 30 * 1000, // 30 seconds
			callback: this.createNewPowerUp,
			callbackScope: this,
			loop: true,
		});
	}

	createNewPowerUp() {
		if (!this.scene || !this.scene.player.alive || this.isFull()) return;
		const type = Configs.powerUps.transformAll;
		const newPowerUp = this.get(type);
		newPowerUp.updatePosition();
	}
}
