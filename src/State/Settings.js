import { sortDescendingObj } from "../Utils/Utils";

import EN from "../Lang/EN";
import PT from "../Lang/PT";

export default class Settings {
	constructor() {
		if (Settings.instance instanceof Settings)
			return Settings.instance;

		this.langOptions = {
			en: EN,
			pt: PT,
		};

		this._language = "en";
		if (localStorage.getItem("language")) this._language = localStorage.getItem("language");
		else localStorage.setItem("language", this._language);
		this.output = this.langOptions[this._language];

		this._isMute = true;
		if (localStorage.getItem("isMute")) this._isMute = localStorage.getItem("isMute") === "true" ? true : false;
		else localStorage.setItem("isMute", this._isMute);

		this._leaderboard = [];
		if (localStorage.getItem("leaderboard")) this._leaderboard = JSON.parse(localStorage.getItem("leaderboard"));
		else localStorage.setItem("leaderboard", JSON.stringify(this._leaderboard));

		Settings.instance = this;
	}

	set language(value) {
		this._language = value;
		this.output = this.langOptions[value];
		localStorage.setItem("language", value);
	}

	set isMute(value) {
		this._isMute = value;
		localStorage.setItem("isMute", value);
	}

	addScore(data) {
		this._leaderboard.push(data);
		localStorage.setItem("leaderboard", JSON.stringify(this._leaderboard));
	}

	sortScore() {
		return sortDescendingObj(this._leaderboard, "leaderboard");
	}

	getTopScore(n) {
		return this.sortScore().slice(0, n);
	}

	getBestScore() { return this.getTopScore(1)[0].score; }

	clearScore() {
		this._leaderboard = [];
		localStorage.setItem("leaderboard", JSON.stringify(this._leaderboard));
	}

	get language() { return this._language; };
	get isMute() { return this._isMute; };
	get score() { return this._leaderboard; };
}
