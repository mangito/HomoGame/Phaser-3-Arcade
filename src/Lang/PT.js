const pt = {
	start: "Jogar",
	language: "Português",
	pause: "Jogo em Pausa",
	continue: "Continuar",
	score: "Pontos",
	bestScore: "Melhor Pontuação",
	selectLevel: "Selecionar a Dificuldade",
	easy: "Fácil",
	normal: "Normal",
	hard: "Difícil",
	back: "Voltar",
	gameOver: "Fim de Jogo",
	playAgain: "Jogar Novamente",
	settings: "Configurações",
	fullScreen: "Tela Cheia",
	sound: "Som",
	leaderboard: "Placar",
	noLeaderboard: "Não há registro de pontuação\nAinda...",
};

export default pt;
